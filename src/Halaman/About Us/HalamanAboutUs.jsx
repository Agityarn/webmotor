import TemplateUtama from "../Komponen/TemplateUtama";
import gear from '../../../assets/gear.jpg'
export default function HalamanAboutUs() {
  return (
    <TemplateUtama>
        <div className="w-50 h-60 p-2 flex items-center justify-center" >
          <img src={gear} />
        </div>
        <div className="w-full h-25 flex items-center" >
        <p>Perkumpuluan Club Honda Supra X adalah sebuah komunitas yang bertujuan untuk menyatukan komunitas Honda yang berada di Malang serta menjadikan para bikers menjadi kreatif dan berkarya. Komunitas ini berada di bawah naungan PT. Astra Honda yang dapat mengayomi 
           anggota komunitas Motor Honda.</p> 
        </div>
        <div className="h-4"/>
        <p>Visi : 
        Menjadikan Komunitas ini suatu wadah organisasi pemersatu komunitas Honda yang berada di Malang</p>
        <div className="h-4"/>
        <p>Misi :</p>
        <p> 1. Merangkul semua komunitas Honda yang berada di Malang agar menjadi komunitas yang bermanfaat dan berguna bagi masyarakat serta menjadi contoh yang baik bagi Komunitas lain dan bagi masyarakat.</p>
        <p> 2. Turut berpartisipasi Dan mengempanyekan Safety Riding kepada komunitas Honda serta masyarakat agar dapat memahami arti keselamatan dalam berkendara.</p>
        <p> 3. Menjadikan Komunitas ini sebagai fasilitator pengembangan komunitas Honda untuk berkreasi dan berkarya untuk menjadi komunitas yang mandiri.</p>
    </TemplateUtama>
  );
}