import TemplateUtama from "../Komponen/TemplateUtama";
import kopdar from '../../../assets/kopdar.jpg'
import kopdar2 from '../../../assets/kopdar2.jpg'
import turing from '../../../assets/turing.jpg'

export default function HalamanGalleryFoto() {
  return (
    <TemplateUtama>
      <div className="w-full flex flex-row ">
      <div className="flex flex-col items-center"><img className="w-98 h-60 p-2"src={kopdar} />
      <p>Kumpul Komunitas Club Honda Supra X</p>
      </div>
      <div className="flex flex-col items-center"> <img className="w-98 h-60 p-2"src={turing} />
      <p>Touring Komunitas Club Honda Supra X</p>
      </div>
      <div className="flex flex-col items-center"> <img className="w-98 h-60 p-2"src={kopdar2} />
      <p>Kumpul Kedua Komunitas Club Honda Supra X</p>
      </div>
      
      </div>
    </TemplateUtama>
  );
}