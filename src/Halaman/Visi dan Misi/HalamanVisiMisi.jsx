import TemplateUtama from "../Komponen/TemplateUtama";

export default function HalamanVisiMisi() {
  return (
    <TemplateUtama>
        <div className="w-full h-10 flex items-center" >
        <p>Visi : 
        Menjadikan Komunitas ini suatu wadah organisasi pemersatu komunitas Honda yang berada di Malang</p>
        </div>
        <div className="w-full h-10  flex items-center" >
        <p>Misi :</p>
        </div>
        <p> 1. Merangkul semua komunitas Honda yang berada di Malang agar menjadi komunitas yang bermanfaat dan berguna bagi masyarakat serta menjadi contoh yang baik bagi Komunitas lain dan bagi masyarakat.</p>
          <p> 2. Turut berpartisipasi Dan mengempanyekan Safety Riding kepada komunitas Honda serta masyarakat agar dapat memahami arti keselamatan dalam berkendara.</p>
          <p> 3. Menjadikan Komunitas ini sebagai fasilitator pengembangan komunitas Honda untuk berkreasi dan berkarya untuk menjadi komunitas yang mandiri.</p>
       
    </TemplateUtama>
  );
}