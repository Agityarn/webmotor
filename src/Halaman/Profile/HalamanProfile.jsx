import TemplateUtama from "../Komponen/TemplateUtama";
import gear from '../../../assets/gear.jpg'
export default function HalamanProfile() {
  return (
    <TemplateUtama>
         <div className="w-50 h-60 p-2 flex items-center justify-center" >
        <img src={gear} />
        </div>
      <div className='h-full w-full'>
        <p>Perkumpuluan Club Honda Supra X adalah sebuah komunitas yang bertujuan untuk menyatukan komunitas Honda yang berada di Malang serta menjadikan para bikers menjadi kreatif dan berkarya. Komunitas ini berada di bawah naungan PT. Astra Honda yang dapat mengayomi 
           anggota komunitas Motor Honda.</p>
      </div>
    </TemplateUtama>
  );
}