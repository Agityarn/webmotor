import TemplateUtama from "../Komponen/TemplateUtama";
import rompi from '../../../assets/rompi.jpg'
import sarungtangan from '../../../assets/sarungtangan.jpg'
import kaki from '../../../assets/kaki.jpg'
export default function HalamanProdukKami() {
  return (
    <TemplateUtama>
    <div className="w-full flex flex-row gap-2 ">
      <div className="flex flex-col items-center"><img className="w-98 h-60 p-2"src={rompi} />
      <p>Rompi Pelindung</p>
      </div>
      <div className="flex flex-col items-center"><img className="w-98 h-60 p-2"src={sarungtangan} />
      <p>Sarung Tangan Motor</p>
      </div>
      <div className="flex flex-col items-center"><img className="w-98 h-60 p-2"src={kaki} />
      <p>Pelindung Kaki Motor</p>
      </div>
      </div>
    </TemplateUtama>
  );
}