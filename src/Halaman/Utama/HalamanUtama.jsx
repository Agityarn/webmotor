import TemplateUtama from '../Komponen/TemplateUtama'
import gear from '../../../assets/gear.jpg'
export default function HalamanUtama() {
  return (
    <TemplateUtama>
        <div className="w-full h-full scale-[2] flex items-center justify-center" >
          <img src={gear} />
        </div>
    </TemplateUtama>
  );
}
