import TemplateUtama from "../Komponen/TemplateUtama";
import astrea from '../../../assets/astrea.png'
import scopy from '../../../assets/scopy.png'

export default function HalamanKlienKamu() {
  return (
    <TemplateUtama>
      <div className="w-full flex flex-row ">
      <div className="flex flex-col items-center"><img className="w-100 h-60 p-2"src={astrea} />
      <p>Honda Astrea Club Malang</p>
      </div>
      <div className="flex flex-col items-center"><img className="w-100 h-60 p-2"src={scopy} />
      <p>Honda Scoopy Riders Club</p>
      </div>
      </div>
    </TemplateUtama>
  );
}